import React from "react";
import { Application } from "./routes/application/application.jsx";

export default function App() {
  return <Application />;
}
