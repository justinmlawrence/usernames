import React from "react";
import { HashRouter as Router, Switch, Route, Link } from "react-router-dom";
import "./heading.scss";

export class Heading extends React.Component {
  state = {
    menuOpen: false,
  };

  toggleMenu = () => {
    this.setState({ menuOpen: !this.state.menuOpen });
  };

  render() {
    const { menuOpen } = this.state;

    return (
      <Router>
        <div className="heading-wrapper">
          <div
            className="heading-mobile-menu-icon-wrapper"
            onClick={() => {
              this.toggleMenu();
            }}
          >
            <img
              alt=""
              src="/assets/images/icons/menu.svg"
              className="heading-mobile-menu-icon"
            />
          </div>
          {menuOpen && (
            <div className="mobile-menu-wrapper">
              <div className="mobile-menu-heading">MENU</div>
              <Link
                to="/"
                className="mobile-menu-button-row"
                onClick={() => {
                  this.toggleMenu();
                }}
              >
                <div className="mobile-menu-icon-wrapper">
                  <div className="mobile-menu-icon-box dashboard">
                    <img
                      alt=""
                      src="/assets/images/icons/grid.svg"
                      className="mobile-menu-icon"
                    />
                  </div>
                </div>
                <div className="mobile-menu-text">
                  <div className="mobile-menu-button-main">Dashboard</div>
                  <div className="mobile-menu-button-sub">
                    Your default listing page
                  </div>
                </div>
              </Link>
              <div className="mobile-menu-heading">USER SETTINGS</div>

              <div className="mobile-menu-button-row">
                <div className="mobile-menu-icon-wrapper">
                  <div className="mobile-menu-icon-box notifications">
                    <img
                      alt=""
                      src="/assets/images/icons/notifications-active.svg"
                      className="mobile-menu-icon"
                    />
                  </div>
                </div>
                <div className="mobile-menu-text">
                  <div className="mobile-menu-button-main">Notifications</div>
                  <div className="mobile-menu-button-sub">
                    You have five (5) notifications
                  </div>
                </div>
              </div>

              <div className="mobile-menu-button-row">
                <div className="mobile-menu-icon-wrapper">
                  <div className="mobile-menu-icon-box messages">
                    <img
                      alt=""
                      src="/assets/images/icons/messages-inactive.svg"
                      className="mobile-menu-icon"
                    />
                  </div>
                </div>
                <div className="mobile-menu-text">
                  <div className="mobile-menu-button-main">Messages</div>
                  <div className="mobile-menu-button-sub">
                    You have no messages
                  </div>
                </div>
              </div>

              <Link
                to="/profile"
                className="mobile-menu-button-row"
                onClick={() => {
                  this.toggleMenu();
                }}
              >
                <div className="mobile-menu-icon-wrapper">
                  <div className="mobile-menu-icon-box profile">
                    <img
                      alt=""
                      src="/assets/images/icons/profile-inactive.svg"
                      className="mobile-menu-icon"
                    />
                  </div>
                </div>
                <div className="mobile-menu-text">
                  <div className="mobile-menu-button-main">View Profile</div>
                  <div className="mobile-menu-button-sub">
                    Look at how your profile is displayed
                  </div>
                </div>
              </Link>
            </div>
          )}
          <div className="heading-page-name">
            <Switch>
              <Route exact path="/">
                Dashboard
                <Link to="/create" className="heading-sell-user-button-wrapper">
                  <div className="heading-sell-username-button">
                    <img
                      alt=""
                      src="/assets/images/icons/add.svg"
                      className="heading-sell-user-name-button-icon"
                    />
                    <span className="heading-sell-user-button-label">
                      Sell Username
                    </span>
                  </div>
                </Link>
              </Route>{" "}
              <Route path="/profile">Profile</Route>{" "}
              <Route path="/create">
                Create New Sale
                <Link to="/" className="heading-back-to-listing-wrapper">
                  <div className="heading-back-to-listing">
                    <img
                      alt=""
                      src="/assets/images/icons/left-chevron.svg"
                      className="heading-back-to-listing-icon"
                    />
                    Back to Listing
                  </div>
                </Link>
              </Route>{" "}
              <Route path="/listing">
                <div className="page-name-text">@username</div>
                <Link to="/" className="heading-back-to-listing-wrapper">
                  <div className="heading-back-to-listing">
                    <img
                      alt=""
                      src="/assets/images/icons/left-chevron.svg"
                      className="heading-back-to-listing-icon"
                    />
                    Back to Listing
                  </div>
                </Link>
              </Route>{" "}
            </Switch>{" "}
          </div>
          <div className="heading-profile-wrapper">
            <Link to="/profile">
              <div className="heading-profile-image-wrapper">
                <img
                  alt=""
                  src="/assets/images/examples/user-2.jpg"
                  className="heading-profile-image"
                />
              </div>
            </Link>
            <div className="heading-username-wrapper">
              <div className="heading-username">zyrlspn</div>
              <div className="heading-username-verified">
                <img
                  alt=""
                  src="/assets/images/icons/verified.svg"
                  className="heading-verified"
                />
                Verified User
              </div>
            </div>
          </div>
          <div className="heading-buttons-wrapper">
            <div className="heading-button">
              <img
                alt=""
                src="/assets/images/icons/notifications-active.svg"
                className="heading-button-icon"
              />
              <div className="heading-alert-dot"></div>
            </div>
            <div className="heading-button">
              <img
                alt=""
                src="/assets/images/icons/messages-inactive.svg"
                className="heading-button-icon"
              />
            </div>
            <div className="heading-button">
              <img
                alt=""
                src="/assets/images/icons/profile-inactive.svg"
                className="heading-button-icon"
              />
            </div>
          </div>
        </div>
        <Route exact path="/">
          <Link to="/create" className="heading-sell-user-button-mobile">
            <img
              alt=""
              src="/assets/images/icons/add-square.svg"
              className="heading-sell-user-name-button-icon-mobile"
            />
          </Link>
        </Route>
      </Router>
    );
  }
}
