import React from "react";
import { RatingStars } from "../rating-stars/rating-stars.jsx";
import "./rating-history-card.scss";

export class RatingHistoryCard extends React.Component {
  render() {
    return (
      <div className="rating-history-card">
        <div className="rating-history-top-row">
          <div className="rating-history-profile-picture-wrapper">
            <img
              alt=""
              src="/assets/images/examples/user-1.jpg"
              className="rating-history-profile-picture"
            />
          </div>
          <div className="rating-history-user-wrapper">
            <div className="rating-history-username">Rowan</div>
            <div className="rating-history-admin">Administrator</div>
          </div>
          <div className="rating-history-rating-wrapper">
            <RatingStars />
          </div>
        </div>
        <div className="rating-history-info-row">
          <div className="rating-history-info-element">
            <div className="profile-subheading">Role</div>
            <div className="rating-history-info-content">Seller</div>
          </div>
          <div className="rating-history-info-element">
            <div className="profile-subheading">Username</div>
            <div className="rating-history-info-content">Seastar</div>
          </div>
          <div className="rating-history-info-element">
            <div className="profile-subheading">Category</div>
            <div className="rating-history-info-content">Instagram</div>
          </div>
          <div className="rating-history-info-element">
            <div className="profile-subheading">Post</div>
            <div className="rating-history-info-content link">Thread Link</div>
          </div>
        </div>
        <div className="profile-subheading">Comment</div>
        <div className="rating-history-comment">
          Cursus platea sed turpis eleifend velit. In libero, id dignissim felis
          sollicitudin cras cras varius. Nulla vulputate eu sed elementum,
          viverra elementum nisi nunc.{" "}
        </div>
        <div className="rating-history-bottom-meta">
          <div className="rating-history-bottom-meta-timestamp">
            August 15, 2020 at 9:51 PM
          </div>
          <div className="rating-history-bottom-meta-report">Report</div>
        </div>
      </div>
    );
  }
}
