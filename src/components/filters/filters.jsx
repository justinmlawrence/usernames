import React from "react";
import { Dropdown } from "../dropdown/dropdown.jsx";
import { SocialDropdownOptions } from "../social-dropdown-options/social-dropdown-options.jsx";
import "./filters.scss";

export class Filters extends React.Component {
  state = {
    sortBy: "newest",
  };

  handleOnClickSort = (sortBy) => {
    this.setState({ sortBy });
  };
  render() {
    const { onAdvancedFiltersClick, advancedFilterActive } = this.props;
    const { sortBy } = this.state;

    return (
      <div className="filters-wrapper">
        <div className="filters-heading">FILTERS</div>
        <div className="filters-dropdowns-wrapper">
          <Dropdown
            desktopLabel="Filter by Social Media"
            mobileLabel="Social Media"
          >
            <SocialDropdownOptions />
          </Dropdown>
          <Dropdown
            desktopLabel={
              sortBy === "newest" ? "Sort by Newest" : "Sort by Alphabetical"
            }
            mobileLabel={sortBy === "newest" ? "Newest" : "Alphabetical"}
          >
            <div className="vertical-dropdown-contents-wrapper">
              <div
                className={`vertical-dropdown-element ${
                  sortBy === "newest" && "active"
                }`}
                onClick={() => {
                  this.handleOnClickSort("newest");
                }}
              >
                Sort by Newest
              </div>
              <div
                className={`vertical-dropdown-element ${
                  sortBy === "alphabetical" && "active"
                }`}
                onClick={() => {
                  this.handleOnClickSort("alphabetical");
                }}
              >
                Sort by Alphabetical
              </div>
            </div>
          </Dropdown>

          <div
            className={`filters-advanced-search-button filter-button ${
              advancedFilterActive ? "active" : "inactive"
            }`}
            onClick={onAdvancedFiltersClick}
          >
            <span className="desktop-label">Advanced Search</span>
            <span className="mobile-label">Search</span>
          </div>
        </div>
      </div>
    );
  }
}
