import React from "react";
import { Dropdown } from "../dropdown/dropdown.jsx";
import { SocialDropdownOptions } from "../social-dropdown-options/social-dropdown-options.jsx";
import "./advanced-search.scss";

export class AdvancedSearch extends React.Component {
  state = {
    priceAffordableIsActive: false,
    priceAverageIsActive: true,
    priceExpensiveIsActive: false,
    sellerStatus: "verified",
  };

  toggleCheckbox = (box) => {
    const {
      priceAffordableIsActive,
      priceAverageIsActive,
      priceExpensiveIsActive,
    } = this.state;

    if (box === "affordable") {
      this.setState({ priceAffordableIsActive: !priceAffordableIsActive });
    } else if (box === "average") {
      this.setState({ priceAverageIsActive: !priceAverageIsActive });
    } else {
      this.setState({ priceExpensiveIsActive: !priceExpensiveIsActive });
    }
  };

  handleOnClickSellerStatus = (status) => {
    this.setState({ sellerStatus: status });
  };

  render() {
    const { onCloseClick } = this.props;
    const {
      priceAverageIsActive,
      priceAffordableIsActive,
      priceExpensiveIsActive,
      sellerStatus,
    } = this.state;

    return (
      <div className="advanced-search-wrapper">
        <div className="advanced-search-heading">
          <div className="advanced-search-heading-text">Search</div>
          <img
            alt=""
            src="/assets/images/icons/close.svg"
            className="advanced-search-heading-close"
            onClick={onCloseClick}
          />
        </div>
        <div className="advanced-search-search-wrapper">
          <div className="advanced-search-search-icon-wrapper">
            <img
              alt=""
              src="/assets/images/icons/search.svg"
              className="advanced-search-search-icon"
            />
          </div>
          <input
            className="advanced-search-search"
            placeholder="Search by Tags"
          />
        </div>
        <div className="advanced-search-body">
          <div className="advanced-search-subheading">PRICE POINTS</div>
          <div className="advanced-search-price-point-row">
            <div
              onClick={() => {
                this.toggleCheckbox("affordable");
              }}
              className={`advanced-search-price-point-checkbox ${
                priceAffordableIsActive && "active"
              }`}
            >
              {priceAffordableIsActive && (
                <img
                  alt=""
                  src="/assets/images/icons/checkbox-check.svg"
                  className="advanced-search-checkbox-check"
                />
              )}
            </div>
            <div className="advanced-search-price-point-price">$</div>
            <div className="advanced-search-price-point-label">
              Affordable Price
            </div>
          </div>
          <div className="advanced-search-price-point-row">
            <div
              onClick={() => {
                this.toggleCheckbox("average");
              }}
              className={`advanced-search-price-point-checkbox ${
                priceAverageIsActive && "active"
              }`}
            >
              {priceAverageIsActive && (
                <img
                  alt=""
                  src="/assets/images/icons/checkbox-check.svg"
                  className="advanced-search-checkbox-check"
                />
              )}
            </div>
            <div className="advanced-search-price-point-price">$$</div>
            <div className="advanced-search-price-point-label">
              Average Price
            </div>
          </div>
          <div className="advanced-search-price-point-row">
            <div
              onClick={() => {
                this.toggleCheckbox("expensive");
              }}
              className={`advanced-search-price-point-checkbox ${
                priceExpensiveIsActive && "active"
              }`}
            >
              {priceExpensiveIsActive && (
                <img
                  alt=""
                  src="/assets/images/icons/checkbox-check.svg"
                  className="advanced-search-checkbox-check"
                />
              )}
            </div>
            <div className="advanced-search-price-point-price">$$$</div>
            <div className="advanced-search-price-point-label">
              Expensive Price
            </div>
          </div>
          <div className="advanced-search-subheading">SOCIAL MEDIA</div>
          <div className="advanced-search-dropdown-wrapper">
            <Dropdown label="Instagram" fullWidth>
              <SocialDropdownOptions />
            </Dropdown>
          </div>
          <div className="advanced-search-subheading">SELLER</div>
          <div className="advanced-search-dropdown-wrapper">
            <Dropdown
              label={
                sellerStatus === "verified" ? "Verified Only" : "Any Seller"
              }
              fullWidth
            >
              <div className="vertical-dropdown-contents-wrapper">
                <div
                  className={`vertical-dropdown-element ${
                    sellerStatus === "verified" && "active"
                  }`}
                  onClick={() => {
                    this.handleOnClickSellerStatus("verified");
                  }}
                >
                  Verified Only
                </div>
                <div
                  className={`vertical-dropdown-element ${
                    sellerStatus === "any" && "active"
                  }`}
                  onClick={() => {
                    this.handleOnClickSellerStatus("any");
                  }}
                >
                  Any Seller
                </div>
              </div>
            </Dropdown>
          </div>

          <div className="advanced-search-execute-search-button">
            Excecute Search
          </div>
        </div>
      </div>
    );
  }
}
