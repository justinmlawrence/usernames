import React from "react";
import "./reputation-half-check.scss";

export class ReputationHalfCheck extends React.Component {
  render() {
    const { size } = this.props;
    return (
      <img
        alt=""
        src="/assets/images/icons/half-check.svg"
        className={`reputation-half-check ${size}`}
      />
    );
  }
}
