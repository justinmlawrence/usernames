import React from "react";
import "./rating-half-star.scss";

export class RatingHalfStar extends React.Component {
  render() {
    return (
      <img
        alt=""
        src="/assets/images/icons/half-star.svg"
        className="rating-half-star"
      />
    );
  }
}
