import React from "react";
import { HashRouter as Router, Link } from "react-router-dom";
import "./side-nav.scss";

export const SideNav = () => {
  return (
    <Router>
      <div className="side-nav-wrapper">
        <div className="side-nav-logo-wrapper">
          <Link to="/">
            <img
              alt=""
              src="/assets/images/icons/logo.svg"
              className="side-nav-logo"
            />
          </Link>
        </div>

        <Link to="/">
          <div className="side-nav-menu-button">
            <img
              alt=""
              src="/assets/images/icons/grid.svg"
              className="side-nav-menu-button-icon"
            />
          </div>
        </Link>
      </div>
    </Router>
  );
};
