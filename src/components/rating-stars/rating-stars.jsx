import React from "react";
import "./rating-stars.scss";
import { RatingStar } from "../rating-star/rating-star.jsx";
import { RatingHalfStar } from "../rating-half-star/rating-half-star.jsx";

export class RatingStars extends React.Component {
  render() {
    return (
      <div className="rating-stars-wrapper">
        <div className="rating-stars-score">4.5</div>
        <div className="rating-stars">
          <RatingStar />
          <RatingStar />
          <RatingStar />
          <RatingStar />
          <RatingHalfStar />
        </div>
      </div>
    );
  }
}
