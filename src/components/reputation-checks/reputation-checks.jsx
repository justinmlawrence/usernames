import React from "react";
import "./reputation-checks.scss";
import { ReputationCheck } from "../reputation-check/reputation-check.jsx";
import { ReputationHalfCheck } from "../reputation-half-check/reputation-half-check.jsx";

export class ReputationChecks extends React.Component {
  render() {
    return (
      <div className="reputation-check-wrapper">
        <div className="reputation-score">4.5</div>
        <div className="reputation-checks">
          <ReputationCheck size="medium" />
          <ReputationCheck size="medium" />
          <ReputationCheck size="medium" />
          <ReputationCheck size="medium" />
          <ReputationHalfCheck size="medium" />
        </div>
      </div>
    );
  }
}
