import React from "react";
import "./social-dropdown-options.scss";

export class SocialDropdownOptions extends React.Component {
  state = {
    instagramFilterActive: true,
    twitterFilterActive: false,
    snapchatFilterActive: false,
    facebookFilterActive: false,
    telegramFilterActive: false,
    tikTokFilterActive: false,
  };

  toggleSocialFilter = (social) => {
    const {
      instagramFilterActive,
      twitterFilterActive,
      snapchatFilterActive,
      facebookFilterActive,
      telegramFilterActive,
      tikTokFilterActive,
    } = this.state;

    switch (social) {
      case "instagram":
        this.setState({ instagramFilterActive: !instagramFilterActive });
        break;

      case "twitter":
        this.setState({ twitterFilterActive: !twitterFilterActive });
        break;

      case "snapchat":
        this.setState({ snapchatFilterActive: !snapchatFilterActive });
        break;

      case "facebook":
        this.setState({ facebookFilterActive: !facebookFilterActive });
        break;

      case "telegram":
        this.setState({ telegramFilterActive: !telegramFilterActive });
        break;

      case "tikTok":
        this.setState({ tikTokFilterActive: !tikTokFilterActive });
        break;

      default:
        console.warn("Social toggle fell through!");
        break;
    }
  };
  render() {
    const {
      instagramFilterActive,
      twitterFilterActive,
      snapchatFilterActive,
      facebookFilterActive,
      telegramFilterActive,
      tikTokFilterActive,
    } = this.state;
    return (
      <div className="social-dropdown-wrapper">
        <div
          onClick={() => {
            this.toggleSocialFilter("instagram");
          }}
          className={`social-dropdown-element ${
            instagramFilterActive && "active"
          }`}
        >
          <img
            alt=""
            src="/assets/images/icons/social/instagram.svg"
            className="social-dropdown-element-icon"
          />
        </div>
        <div
          onClick={() => {
            this.toggleSocialFilter("twitter");
          }}
          className={`social-dropdown-element ${
            twitterFilterActive && "active"
          }`}
        >
          <img
            alt=""
            src="/assets/images/icons/social/twitter.svg"
            className="social-dropdown-element-icon"
          />
        </div>
        <div
          onClick={() => {
            this.toggleSocialFilter("snapchat");
          }}
          className={`social-dropdown-element ${
            snapchatFilterActive && "active"
          }`}
        >
          <img
            alt=""
            src="/assets/images/icons/social/snapchat.svg"
            className="social-dropdown-element-icon"
          />
        </div>
        <div
          onClick={() => {
            this.toggleSocialFilter("facebook");
          }}
          className={`social-dropdown-element ${
            facebookFilterActive && "active"
          }`}
        >
          <img
            alt=""
            src="/assets/images/icons/social/facebook.svg"
            className="social-dropdown-element-icon"
          />
        </div>
        <div
          onClick={() => {
            this.toggleSocialFilter("telegram");
          }}
          className={`social-dropdown-element ${
            telegramFilterActive && "active"
          }`}
        >
          <img
            alt=""
            src="/assets/images/icons/social/telegram.svg"
            className="social-dropdown-element-icon"
          />
        </div>
        <div
          onClick={() => {
            this.toggleSocialFilter("tikTok");
          }}
          className={`social-dropdown-element ${
            tikTokFilterActive && "active"
          }`}
        >
          <img
            alt=""
            src="/assets/images/icons/social/tik_tok.svg"
            className="social-dropdown-element-icon"
          />
        </div>
      </div>
    );
  }
}
