import React, { Fragment } from "react";
import "./dropdown.scss";

export class Dropdown extends React.Component {
  state = {
    showDropdownContents: false,
  };

  toggleDropdown = () => {
    const { showDropdownContents } = this.state;
    this.setState({ showDropdownContents: !showDropdownContents });
  };

  render() {
    let { label, desktopLabel, mobileLabel, fullWidth, children } = this.props;
    const { showDropdownContents } = this.state;

    if (label) {
      desktopLabel = label;
      mobileLabel = label;
    }

    return (
      <Fragment>
        <div
          className={`filters-filter-button filter-button ${
            fullWidth ? "full-width" : ""
          }`}
          onClick={() => {
            this.toggleDropdown();
          }}
        >
          <span className="dropdown-desktop-label">{desktopLabel}</span>
          <span className="dropdown-mobile-label">{mobileLabel}</span>
          <img
            alt=""
            src="/assets/images/icons/down-chevron.svg"
            className="filters-filter-button-icon"
          />
        </div>
        {showDropdownContents && children && (
          <Fragment>
            <div
              className="dropdown-contents-close-button"
              onClick={() => {
                this.toggleDropdown();
              }}
            >
              <img
                alt=""
                className="dropdown-contents-close-button-icon"
                src="/assets/images/icons/close.svg"
              />
            </div>
            {children}
            {/* <div className="dropdown-contents-wrapper">
                </div> */}
            <div className="dropdown-gauze"></div>
          </Fragment>
        )}
      </Fragment>
    );
  }
}
