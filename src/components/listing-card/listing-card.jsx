import React from "react";
import "./listing-card.scss";

export class ListingCard extends React.Component {
  render() {
    const { username } = this.props;

    return (
      <div className="listing-card-wrapper">
        <div className="listing-card-heading">
          <div className="listing-card-username">{username}</div>
          <div className="listing-card-social">
            <img
              alt=""
              src="/assets/images/icons/social/facebook.svg"
              className="listing-card-social-icon"
            />
            <div className="listing-card-social-name">Facebook</div>
          </div>
        </div>
        <div className="listing-card-info-row">
          <div className="listing-card-info-element">
            <div className="profile-subheading">Type</div>
            <div className="listing-card-info-content">Auction Sale</div>
          </div>
          <div className="listing-card-info-element">
            <div className="profile-subheading">Sale Price</div>
            <div className="listing-card-info-content">$$</div>
          </div>
          <div className="listing-card-info-element">
            <div className="profile-subheading">Time Left</div>
            <div className="listing-card-info-content">
              25 minutes 33 seconds
            </div>
          </div>
          <div className="listing-card-info-element">
            <div
              className="profile-listing-view-post-button"
              title="View this post"
            >
              View Post
            </div>
          </div>
        </div>
      </div>
    );
  }
}
