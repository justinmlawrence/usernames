import React from "react";
import "./reputation-check.scss";

export class ReputationCheck extends React.Component {
  render() {
    const { size } = this.props;
    return (
      <img
        alt=""
        src="/assets/images/icons/check.svg"
        className={`reputation-check ${size}`}
      />
    );
  }
}
