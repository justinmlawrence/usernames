import React from "react";
import { ReputationChecks } from "../reputation-checks/reputation-checks.jsx";
import "./reputation-history-card.scss";

export class ReputationHistoryCard extends React.Component {
  render() {
    return (
      <div className="reputation-history-card">
        <div className="reputation-history-top-row">
          <div className="reputation-history-profile-picture-wrapper">
            <img
              alt=""
              src="/assets/images/examples/user-1.jpg"
              className="reputation-history-profile-picture"
            />
          </div>
          <div className="reputation-history-user-wrapper">
            <div className="reputation-history-username">Rowan</div>
            <div className="reputation-history-admin">Administrator</div>
          </div>
          <div className="reputation-history-reputation-wrapper">
            <ReputationChecks />
          </div>
        </div>
        <div className="profile-subheading">Comment</div>
        <div className="reputation-history-comment">
          Cursus platea sed turpis eleifend velit. In libero, id dignissim felis
          sollicitudin cras cras varius. Nulla vulputate eu sed elementum,
          viverra elementum nisi nunc.{" "}
        </div>
      </div>
    );
  }
}
