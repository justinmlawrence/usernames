import React from "react";
import "./rating-star.scss";

export class RatingStar extends React.Component {
  render() {
    return (
      <img alt="" src="/assets/images/icons/star.svg" className="rating-star" />
    );
  }
}
