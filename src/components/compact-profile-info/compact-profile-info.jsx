import React from "react";
import "./compact-profile-info.scss";
import { ReputationCheck } from "../reputation-check/reputation-check.jsx";
import { RatingStar } from "../rating-star/rating-star.jsx";

export class CompactProfileInfo extends React.Component {
  render() {
    return (
      <div className="compact-profile-info-wrapper">
        <div className="compact-profile-info-element small">
          <div className="compact-profile-info-element-content text bold">
            Rowan
          </div>
          <div className="compact-profile-info-element-label">Username</div>
        </div>
        <div className="compact-profile-info-element large">
          <div className="compact-profile-info-element-content text">
            March 31, 2020
          </div>
          <div className="compact-profile-info-element-label">Join Date</div>
        </div>
        <div className="compact-profile-info-element small">
          <div className="compact-profile-info-element-content text">
            Member
          </div>
          <div className="compact-profile-info-element-label">Rank</div>
        </div>
        <div className="compact-profile-info-element small">
          <div className="compact-profile-info-element-content">
            <div className="compact-profile-info-rating-number rating">4.5</div>
            <div className="compact-profile-info-rating-wrapper">
              <RatingStar />
            </div>
            <div className="compact-profile-info-rating-count">(55)</div>
          </div>
          <div className="compact-profile-info-element-label">Vouches</div>
        </div>
        <div className="compact-profile-info-element large">
          <div className="compact-profile-info-element-content">
            <div className="compact-profile-info-rating-number reputation">
              4.5
            </div>
            <div className="compact-profile-info-rating-wrapper">
              <ReputationCheck />
            </div>
            <div className="compact-profile-info-rating-count">(3)</div>
          </div>
          <div className="compact-profile-info-element-label">Reputation</div>
        </div>
      </div>
    );
  }
}
