import React from "react";
import { HashRouter as Router, Link } from "react-router-dom";
import "./for-sale-post.scss";

export const ForSalePost = (props) => {
  const { username, cost, imageUrl } = props;

  return (
    <Router>
      <Link to="/listing" className="for-sale-post-wrapper">
        <div className="for-sale-post-heading-wrapper">
          <div className="for-sale-post-username">{username}</div>
          <div className="for-sale-post-cost">{cost}</div>
          <div className="for-sale-post-profile-picture-wrapper">
            <img
              alt=""
              src={`/assets/images/examples/${imageUrl}`}
              className="for-sale-post-profile-picture"
            />
          </div>
        </div>
        <div className="for-sale-post-message">
          Id deserunt consectetur amet quis ut proident fugiat pariatur
          incididunt consectetur nulla ullamco voluptate cillum. Fugiat nisi ad
          id eu id. Id ullamco fugiat et pariatur commodo adipisicing.
        </div>
        <div className="tag-wrapper">
          <div className="tag social twitter">Twitter</div>
          <div className="tag verified">
            <img
              alt=""
              src="/assets/images/icons/verified-white.svg"
              className="verified-icon"
            />
            Verified User
          </div>
        </div>
      </Link>
    </Router>
  );
};
