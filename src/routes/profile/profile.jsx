import React, { Fragment } from "react";
import "./profile.scss";
import { RatingStars } from "../../components/rating-stars/rating-stars.jsx";
import { ReputationChecks } from "../../components/reputation-checks/reputation-checks.jsx";
import { ReputationHistoryCard } from "../../components/reputation-history-card/reputation-history-card.jsx";
import { RatingHistoryCard } from "../../components/rating-history-card/rating-history-card.jsx";
import { ListingCard } from "../../components/listing-card/listing-card.jsx";

export class Profile extends React.Component {
  state = {
    activeTab: "about",
  };

  clickTab = (tab) => {
    this.setState({ activeTab: tab });
  };

  render() {
    const { activeTab } = this.state;
    const isAboutTab = activeTab === "about";
    const isReputationTab = activeTab === "reputation";
    const isVouchesTab = activeTab === "vouches";
    const isListingsTab = activeTab === "listings";
    return (
      <div className="page-contents">
        <div className="profile-heading">
          <div className="profile-photo-wrapper">
            <img
              src="/assets/images/examples/user-2.jpg"
              alt="Profile"
              className="profile-photo"
            />
          </div>
          <div className="profile-heading-user-info">
            <div className="profile-heading-username">
              zyrlspn{" "}
              <img
                alt=""
                src="/assets/images/icons/verified.svg"
                className="profile-heading-username-verified"
              />
            </div>
            <div className="profile-heading-online">
              <div className="profile-heading-online-icon"></div>Online
            </div>
          </div>
          <div className="profile-heading-buttons">
            <div className="profile-heading-button reputation inactive">
              <img
                alt=""
                src="/assets/images/icons/check.svg"
                className="profile-heading-button-icon"
              />
              Add Reputation
            </div>
            <div className="profile-heading-button message">Send Message</div>
          </div>
        </div>
        <div className="profile-tab-row">
          <div
            className={`profile-tab ${isAboutTab ? "active" : ""}`}
            title="Learn more about this user"
            onClick={() => {
              this.clickTab("about");
            }}
          >
            ABOUT
          </div>
          <div
            className={`profile-tab ${isReputationTab ? "active" : ""}`}
            title="View this users's Reputation"
            onClick={() => {
              this.clickTab("reputation");
            }}
          >
            REPUTATION
          </div>
          <div
            className={`profile-tab ${isVouchesTab ? "active" : ""}`}
            title="View this users's Vouches"
            onClick={() => {
              this.clickTab("vouches");
            }}
          >
            VOUCHES
          </div>
          <div
            className={`profile-tab ${isListingsTab ? "active" : ""}`}
            title="View this users's Active Listings"
            onClick={() => {
              this.clickTab("listings");
            }}
          >
            ACTIVE LISTING (2)
          </div>
        </div>
        <div className="profile-body">
          {isAboutTab && (
            <Fragment>
              <div className="profile-info-row">
                <div className="profile-info-element wide">
                  <div className="profile-subheading">User ID</div>
                  <div className="profile-info-element-body">USER0001</div>
                </div>
                <div className="profile-info-element">
                  <div className="profile-subheading">User Rank</div>
                  <div className="profile-info-element-body verified">
                    Verified User
                  </div>
                </div>
                <div className="profile-info-element">
                  <div className="profile-subheading">Join Date</div>
                  <div className="profile-info-element-body">
                    March 31, 2020
                  </div>
                </div>
                <div className="profile-info-element">
                  <div className="profile-subheading">Discord</div>
                  <div className="profile-info-element-body">zyrl#0012</div>
                </div>
              </div>
              <div className="profile-subheading">Bio</div>
              <div className="about-tab-bio-content">
                Tempor tellus dolor nunc sit quam viverra non posuere. Magna sed
                nulla diam blandit. Eget at malesuada purus amet massa vitae
                elementum dolor dolor. Tellus sed sollicitudin nisl scelerisque.
              </div>

              <div className="about-tab-callout-box-row">
                <div className="about-tab-callout-box">
                  <div className="about-tab-callout-box-heading">Vouches</div>
                  <div className="about-tab-callout-box-rating-row">
                    <RatingStars />
                    <div className="about-tab-callout-box-rating-count">
                      (55)
                    </div>
                  </div>
                </div>
                <div className="about-tab-callout-box">
                  <div className="about-tab-callout-box-heading">
                    Reputation
                  </div>
                  <div className="about-tab-callout-box-rating-row">
                    <ReputationChecks />
                    <div className="about-tab-callout-box-rating-count">
                      (3)
                    </div>
                  </div>
                </div>
              </div>
            </Fragment>
          )}
          {isReputationTab && (
            <Fragment>
              <div className="profile-info-row">
                <div className="profile-info-element wide">
                  <div className="profile-subheading">Total Score</div>
                  <div className="profile-info-element-body">
                    <ReputationChecks />
                  </div>
                </div>
                <div className="profile-info-element">
                  <div className="profile-subheading">Total Reputation</div>
                  <div className="profile-info-element-body">25</div>
                </div>
                <div className="profile-info-element">
                  <div className="profile-subheading">Recent Reputation</div>
                  <div className="profile-info-element-body">Rowan</div>
                </div>
              </div>
              <div className="profile-subheading">Reputation History</div>
              <ReputationHistoryCard />
            </Fragment>
          )}
          {isVouchesTab && (
            <Fragment>
              <div className="profile-info-row">
                <div className="profile-info-element wide">
                  <div className="profile-subheading">Total Score</div>
                  <div className="profile-info-element-body">
                    <RatingStars />
                  </div>
                </div>
                <div className="profile-info-element">
                  <div className="profile-subheading">Total Vouch</div>
                  <div className="profile-info-element-body">25</div>
                </div>
                <div className="profile-info-element">
                  <div className="profile-subheading">Recent Vote</div>
                  <div className="profile-info-element-body">Rowan</div>
                </div>
              </div>
              <div className="profile-subheading">Vouch History</div>
              <RatingHistoryCard />
            </Fragment>
          )}
          {isListingsTab && (
            <Fragment>
              <ListingCard username="@username" />
              <ListingCard username="@seastars" />
            </Fragment>
          )}
        </div>
      </div>
    );
  }
}
