import React, { Fragment } from "react";
import "./dashboard.scss";
import { Filters } from "../../components/filters/filters.jsx";
import { ForSalePost } from "../../components/for-sale-post/for-sale-post.jsx";
import { AdvancedSearch } from "../../components/advanced-search/advanced-search.jsx";

export class Dashboard extends React.Component {
  state = {
    showAdvancedFilters: false,
  };

  onAdvancedFiltersClick = () => {
    console.log("clicking to show advanced filters click!");
    this.setState({ showAdvancedFilters: !this.state.showAdvancedFilters });
  };

  render() {
    const { showAdvancedFilters } = this.state;
    return (
      <div className="page-contents">
        <Fragment>
          <Filters
            onAdvancedFiltersClick={this.onAdvancedFiltersClick}
            advancedFilterActive={showAdvancedFilters}
          />
          <div className="dashboard-feed-column">
            <ForSalePost
              username="@username"
              cost="$$$"
              imageUrl="user-2.jpg"
            />
            <ForSalePost
              username="/gaminggirl"
              cost="$$"
              imageUrl="user-1.jpg"
            />
          </div>
          {showAdvancedFilters && (
            <div className="dashboard-advanced-filters-column">
              <AdvancedSearch onCloseClick={this.onAdvancedFiltersClick} />
            </div>
          )}
        </Fragment>
      </div>
    );
  }
}
