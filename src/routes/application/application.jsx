import React from "react";
import { HashRouter as Router, Switch, Route } from "react-router-dom";

import { Profile } from "../profile/profile.jsx";
import { Create } from "../create/create.jsx";
import { Listing } from "../listing/listing.jsx";
import { Dashboard } from "../dashboard/dashboard.jsx";
import { SideNav } from "../../components/side-nav/side-nav.jsx";
import { Heading } from "../../components/heading/heading.jsx";
import "./application.scss";

export class Application extends React.Component {
  render() {
    return (
      <Router>
        <SideNav />
        <div className="main-content">
          <Heading />
          <Switch>
            <Route exact path="/">
              <Dashboard />
            </Route>{" "}
            <Route path="/profile">
              <Profile />
            </Route>{" "}
            <Route path="/create">
              <Create />
            </Route>{" "}
            <Route path="/listing">
              <Listing />
            </Route>{" "}
          </Switch>{" "}
        </div>
      </Router>
    );
  }
}
