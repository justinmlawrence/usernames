import React from "react";
import "./listing.scss";
import { HashRouter as Router, Link } from "react-router-dom";
import { RatingStars } from "../../components/rating-stars/rating-stars.jsx";
import { ReputationChecks } from "../../components/reputation-checks/reputation-checks.jsx";
import { CompactProfileInfo } from "../../components/compact-profile-info/compact-profile-info";

export class Listing extends React.Component {
  render() {
    return (
      <Router>
        <div className="listing-main-wrapper">
          <Link to="/" className="listing-back-to-listing-wrapper">
            <img
              alt=""
              src="/assets/images/icons/left-chevron.svg"
              className="listing-back-to-listing-icon"
            />
            Back to Listing
          </Link>
          <div className="listing-body-column">
            <div className="listing-listing-card-wrapper">
              <div className="listing-listing-row">
                <div className="listing-listing-card-heading">@username</div>
                <div className="listing-listing-card-social">Instagram</div>
                <div className="listing-listing-card-social-icon-wrapper">
                  <img
                    alt=""
                    src="/assets/images/icons/social/instagram.svg"
                    className="listing-listing-card-social-icon-wrapper"
                  />
                </div>
              </div>
              <div className="listing-listing-row">
                <div className="listing-listing-price-box">
                  <div className="listing-listing-price-box-price">$250</div>
                  <div className="listing-listing-price-box-price-label">
                    PRICE
                  </div>
                </div>
              </div>
              <div className="listing-listing-row">
                <div className="listing-listing-subheading">DESCRIPTION</div>
              </div>
              <div className="listing-listing-row">
                <div className="listing-listing-description">
                  Tristique metus nunc, sem viverra cras nec egestas faucibus
                  lectus. Pharetra eget volutpat eu lorem ultricies turpis elit
                  aliquam.
                </div>
              </div>
              <div className="listing-listing-row">
                <div className="listing-listing-purchase-button">Purchase</div>
              </div>
              <div className="listing-listing-row">
                <div className="listing-listing-tags-wrapper top">
                  <div className="listing-listing-tags">
                    <span className="tag-special">instagram</span>, gaming,
                    normal, 4char, original
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="listing-author-column">
            <div className="listing-listing-card-wrapper author">
              <div className="listing-author-top-info-wrapper">
                <div className="listing-author-top-info-image-wrapper">
                  <img
                    alt=""
                    src="/assets/images/examples/user-1.jpg"
                    className="listing-author-top-info-image"
                  />
                </div>
                <div className="listing-author-top-info-right">
                  <div className="listing-author-top-info-message-button-wrapper">
                    <div className="listing-author-top-info-message-button">
                      Message
                    </div>
                  </div>
                  <div className="listing-author-top-info-online-status-wrapper">
                    <div className="listing-author-top-info-online-status">
                      <div className="listing-author-top-info-online-icon"></div>
                      Online
                    </div>
                  </div>
                </div>
              </div>
              <div className="listing-listing-profile-info-wrapper">
                <div className="listing-listing-author-name">Rowan</div>
                <div className="listing-listing-label">Member</div>
                <div className="listing-listing-join-date">March 31, 2020</div>
                <div className="listing-listing-label">Join Date</div>
                <RatingStars />
                <div className="listing-listing-label">Vouches</div>
                <ReputationChecks />
                <div className="listing-listing-label">Reputation</div>
              </div>
              <div className="listing-listing-profile-info-wrapper-compact">
                <CompactProfileInfo />
              </div>
            </div>
            <div className="listing-listing-tags-wrapper bottom">
              <div className="listing-listing-subheading">TAGS</div>
              <div className="listing-listing-tags">
                <span className="tag-special">instagram</span>, gaming, normal,
                4char, original
              </div>
            </div>
          </div>
          <div className="action-buttons-row">
            <div className="action-button-message">Message</div>
            <div className="action-button-purchase">Purchase</div>
          </div>
        </div>
      </Router>
    );
  }
}
