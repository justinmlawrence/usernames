import React from "react";
import { Dropdown } from "../../components/dropdown/dropdown.jsx";
import { SocialDropdownOptions } from "../../components/social-dropdown-options/social-dropdown-options.jsx";
import "./create.scss";

export class Create extends React.Component {
  state = {
    price: "1500",
  };

  onChangePrice = (e) => {
    this.setState({ price: e.target.value });
  };

  render() {
    const { price } = this.state;

    return (
      <div className="page-contents">
        <div className="create-new-sale-wrapper">
          <div className="create-new-sale-top-row">
            <div className="create-new-sale-top-row-section-wrapper">
              <div className="create-new-sale-subheading">USERNAME</div>
              <input className="create-new-sale-input" placeholder="Username" />
            </div>
            <div className="create-new-sale-top-row-section-wrapper">
              <div className="create-new-sale-subheading">SOCIAL MEDIA</div>
              <div className="create-new-sale-dropdown-wrapper">
                <Dropdown label="Facebook" fullWidth>
                  <SocialDropdownOptions />
                </Dropdown>
              </div>
            </div>
            <div className="create-new-sale-top-row-section-wrapper">
              <div className="create-new-sale-subheading">PRICE</div>
              <input
                className="create-new-sale-input"
                placeholder="Price"
                value={price}
                onChange={this.onChangePrice}
              />
            </div>
          </div>
          <div className="create-new-sale-subheading">DESCRIPTION</div>
          <textarea
            className="create-new-sale-description"
            placeholder="Write a sample of a description here ..."
          />
          <div className="create-new-sale-subheading">TAGS</div>
          <input
            className="create-new-sale-tag-input"
            placeholder="Type tags ..."
          />
          <div className="create-new-sale-tag-instructions">
            Press enter to add the word to the tags.
          </div>
          <div className="tag-wrapper">
            <div className="tag social facebook">Facebook</div>
            <div className="tag inactive">Gaming</div>
            <div className="tag inactive">Normal Tags</div>
          </div>

          <div className="create-new-sale-create-button">
            Create<span className="hide-on-desktop"> New Sale</span>
          </div>
        </div>
      </div>
    );
  }
}
